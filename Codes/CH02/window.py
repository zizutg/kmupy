'''
Created on Sep 8, 2019

@author: user
'''
from Codes.CH02.ezgraphics import GraphicsWindow

win = GraphicsWindow(200, 300)
win.setTitle("Rectangle")

canvas = win.canvas()
canvas.setFill("green")

canvas.drawRect(25, 30, 40, 80)

win.wait()
