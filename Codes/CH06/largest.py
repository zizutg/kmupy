##
#  This program reads a sequence of values and prints them, marking the 
#  largest value.
#

# Create an empty list.
values = []

# Read the input values.
print("Please enter values, Q to quit:")
userInput = input("")
while userInput.upper() != "Q" :
   values.append(float(userInput))
   userInput = input("")
         
# Print all values, marking the largest.
for element in values :
   print(element, end="")
   if element == max(values) :
       print(" <== largest value", end="")
   if element == min(values) :
        print(" <== smallest value", end="")
   print()

