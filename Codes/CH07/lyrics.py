'''
Created on Nov 3, 2019

@author: user
'''
#Open file for reading
inputFile = open("lyrics.txt", "r")

#Read each line using the "in" operator
for line in inputFile :
    
#remove \n (new line) using rstrip
    line = line.rstrip()
    
#Split the line into words 
    wordList = line.split()
    for word in wordList :
        word = word.rstrip(".,?!")
        print(word)

inputFile.close()
