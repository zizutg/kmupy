'''
Created on Nov 28, 2019

@author: user
'''
class MathTeacher:
    
    def __init__(self):
        self.profession = ""
        self.age = 0
    
    def display(self):
        print("My profession is ", self.profession)
        print("My age is ", self.age)
        print("I can walk")
        print("I can talk")
    
    
    def setProfesson(self, profession):
        self.profession = profession
        
    def setAge(self, age):
        self.age = age
         
    def teach(self):
        print("I can Teach Math")
        
def main():
    
    mt = MathTeacher()
    
    mt.setProfesson("Math Teacher")
    mt.setAge(28)
    mt.display()
    mt.teach()    
    
main()  