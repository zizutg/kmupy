##
#  This program shows a simple quiz with one question.
#

from Codes.CH10.questions import Question

# Create the question and expected answer.
q = Question()

q.setText("Who is the inventor of Python?")
q.setAnswer("Yalew")      

# Display the question and obtain user's response.
q.display()
response = input("Your answer: ")
print(q.checkAnswer(response))

