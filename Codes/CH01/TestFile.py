#===============================================================================
# '''
# Created on Aug 30, 2019
# 
# @author: user
# '''
# #===============================================================================
# # # My first Python program
# # print("Hello World!")
# #===============================================================================
# def fill(sq):
#     for i in range(10):
#         sq.append(i*i)
# 
# square = []
# fill(square)
# print(square)

print('{:04}'.format(1))
# words = ["test", "test2", "test2","test3" ]
# setw = set(words)
# print(words)
# print(setw)
# print(sorted(setw))
# setw.add("test4")
# print(setw)
# setw.add("test")
# print(setw)


# i = 0
# found = False
# while i < 20 and found != True :
#    sum = i * 2 + i * 3
#    print(sum, end=" ")
#    if sum > 50 :
#       found = True
#    i = i + 1
# 
# print()
# 
# for i in range(3) :
#    for j in  range(5) :
#       if i % 2 ==  j % 2 :
#          print("*", end="")
#       else :
#          print("-", end="")
#    print()

# def main() :
#    i = 20
#    b = mysteriousFunction2(i)
#    print(b + i)
# 
# def mysteriousFunction1(i) :
#    n = 0
#    while n * n <= i :
#       n += 1
#       print(n, end="")
#    return n - 1
# 
# def mysteriousFunction2(a) :
#    b = 0
#    for n in range(a) :
#       i = mysteriousFunction1(n)
#       b = b + i
#       print(" ", n, i, b)
#    return b
# 
# main()


# def main1() :
#    print("fun(2) =", fun(2))
# 
# def fun(a) :
#    returnValue = 0
#    if a > 5 :
#       returnValue = a
#    else :
#       returnValue = fun(2 * a)
#    return returnValue
# 
# main1() 

# 
# ##
# #  Sample Program that demonstrates the print function
# #
# #  Prints 7
# 
# #===============================================================================
# # print(3 + 4)
# # 
# # #  Print Hello World! on two lines
# # print("Hello")
# # print("World!")
# # 
# # #  Print multiple values with a single print function call
# # print("My favorite number are", 3 + 4, "and", 3 + 10)
# # 
# # #  Print Hello World! on two lines
# # print("Goodbye")
# # print()
# # print("Hope to see you again")
# #===============================================================================
# 
# taxRate = 5 #int
# print("Integer", taxRate)
# taxRate = 5.5 #float
# print("Double", taxRate)
# taxRate = "helo" #string
# print("String", taxRate + " Zee")
#===============================================================================