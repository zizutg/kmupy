'''
Created on Sep 20, 2019

@author: user
'''
orignalPrice = int(input("Type original price: "))

if orignalPrice < 128:
    discountRate = 0.84
else:
    discountRate = 0.92

discountedPrice = discountRate * orignalPrice

print("The discounted price is: ", discountedPrice)